<?php
class Player{
   function __construct($username, $symbol){
      $this->username = $username;
      $this->symbol = $symbol;
      $this->placements = array();
   }
}

class GameField{
   function __construct(){
      $this->field = "  =============\n0.|   |   |   |\n  =============\n1.|   |   |   |\n  =============\n2.|   |   |   |\n  =============\n    0.  1.  2. \n";
   }

   function show($curr_player){
      // prints the current gamefield.
      echo "player: ".$curr_player."\n".$this->field;
   }

   function win_cond($P){
      // returns True, if the current player has won.
      $symb = $P->symbol;
      // horizontals
      if ($this->field[20] == $symb and $this->field[24] == $symb and $this->field[28] == $symb) return True;
      elseif ($this->field[52] == $symb and $this->field[56] == $symb and $this->field[60] == $symb) return True;
      elseif ($this->field[84] == $symb and $this->field[88] == $symb and $this->field[92] == $symb) return True;
      // diagonals
      elseif ($this->field[20] == $symb and $this->field[56] == $symb and $this->field[92] == $symb) return True;
      elseif ($this->field[28] == $symb and $this->field[56] == $symb and $this->field[84] == $symb) return True;
      // vertical
      elseif ($this->field[20] == $symb and $this->field[52] == $symb and $this->field[84] == $symb) return True;
      elseif ($this->field[24] == $symb and $this->field[56] == $symb and $this->field[92] == $symb) return True;
      elseif ($this->field[28] == $symb and $this->field[60] == $symb and $this->field[92] == $symb) return True;
      else return False;
   }

   function free($x, $y){
      // checks, if the box is free
      if ($this->field[((20+32*$x)+4*$y)] == "X" or $this->field[((20+32*$x)+4*$y)] == "O"){
         echo "Field is not free\n"; return False;
      }
      else return True;
   }

   function set_symbol($x, $y, $P){
      // place the player symbol in the selected box
      $this->field[((20+32*$x)+4*$y)] = $P->symbol;
      return $this->win_cond($P);
   }
}

function game(){
   $Player1 = new Player("Nils", "X");
   $Player2 = new Player("Philipp", "O");
   $GF = new GameField();

   $players = array($Player1, $Player2);
   $play = True;

   for ($i=0; $play; $i++){
      $curr_player = $players[$i%count($players)];
      $GF->show($curr_player->username);
      echo "where do you want to place ".$curr_player->symbol."\n";

      $valid = False;
      while (!$valid){
         echo "x: "; $x = fgets(STDIN);
         echo "y: "; $y = fgets(STDIN);
         $valid = $GF->free($x, $y);
      }
      if ($GF->set_symbol($x, $y, $curr_player)) break;
      system('clear');
   }
   system('clear');
   $GF->show($curr_player->username); echo "Player ".$curr_player->username." has won.\n";
}

game();
?>
